import 'package:flutter/material.dart';

const primaryColor = Color(0xFF2697FF);
const secondaryColor = Color(0xFF2A2D3E);
const bgColor = Color(0xFF212332);



//const bgColorLight = Color(0xFF1B5FFF);
//const secondaryColorLight = Color(0xFFFFA826);


const bgColorLight = Colors.white;
const secondaryColorLight = Color(0xFF1B5FFF);

const defaultPadding = 16.0;