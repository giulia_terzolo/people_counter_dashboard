import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../controllers/MenuController.dart';
import '../responsive.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../constants.dart';

class HeaderDash extends StatelessWidget {
  const HeaderDash({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {





    return Row(
      children: [

        if (!Responsive.isMobile(context))
          Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),

        if (!Responsive.isMobile(context))
          const GradientText(
          'People Counter',
          style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
          gradient: LinearGradient(colors: [
            Color(0xffFB8964),
            Color(0xffFA696C),
          ]),
        ),

        const Expanded(child: LogoField()),

        if (!Responsive.isMobile(context))
          Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),

      ],
    );
  }
}







class GradientText extends StatelessWidget {
  const GradientText(
      this.text, {
        required this.gradient,
        this.style,
      });

  final String text;
  final TextStyle? style;
  final Gradient gradient;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      blendMode: BlendMode.srcIn,
      shaderCallback: (bounds) => gradient.createShader(
        Rect.fromLTWH(0, 0, bounds.width, bounds.height),
      ),
      child: Text(text, style: style),
    );
  }
}






class LogoField extends StatelessWidget {
  const LogoField({
    Key? key,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {

    final Color color1 = const Color(0xffFA696C);
    final Color color2 = const Color(0xffFA8165);
    final Color color3 = const Color(0xffFB8964);

    return Container(
            height: 100,
            width: 100,
            padding: const EdgeInsets.all(defaultPadding * 0.75),
            margin: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [color1, color2, color3]
              ),
              boxShadow: [
                BoxShadow(
                    color: color2,
                    offset: const Offset(4.0,4.0),
                    blurRadius: 10.0
                ),
                BoxShadow(
                    color: color3,
                    offset: const Offset(4.0,4.0),
                    blurRadius: 20.0
                ),
                BoxShadow(
                    color: color1,
                    offset: const Offset(4.0,4.0),
                    blurRadius: 30.0
                )
              ],
              //color: primaryColor,
              //borderRadius: BorderRadius.all(Radius.circular(10)),
              shape: BoxShape.circle,
            ),

            child: const Image(image: AssetImage('assets/images/logo.png'),  ),
    );
  }
}