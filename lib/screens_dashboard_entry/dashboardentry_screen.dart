import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:peoplecounter_dashoboard/screens_dashboard_entry/research.dart';
import '../../constants.dart';
import 'dashHeader.dart';
import 'list_stops.dart';


class DashEntryScreen extends StatefulWidget {
  final stopInfo;
  final stopInfoString;
  DashEntryScreen({Key?key,this.stopInfo, this.stopInfoString}) : super(key: key);
  @override
  _DashEntryScreenState createState() =>  _DashEntryScreenState();
}



class _DashEntryScreenState extends State<DashEntryScreen> {


  @override
  Widget build(BuildContext context) {



    return SafeArea(
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            HeaderDash(),
            const SizedBox(height: 50),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(width: defaultPadding),
                Expanded(
                  flex: 5,
                  child: Column(
                    children: [
                      SizedBox(height: defaultPadding),
                      Research(stopInfo: widget.stopInfo,stopInfoString: widget.stopInfoString,),
                      SizedBox(height: defaultPadding),
                      ListStops(stopInfo: widget.stopInfo,stopInfoString: widget.stopInfoString,),

                    ],
                  ),
                ),
                SizedBox(width: defaultPadding),

              ],
            )
          ],
        ),
      ),
    );
  }
}