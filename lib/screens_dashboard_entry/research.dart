
import 'package:flutter/foundation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import '../../../constants.dart';
import '../jsonFilesHandling/cityStopJson.dart';
import '../responsive.dart';
import '../screens_main/main_screen.dart';
import 'package:dropdown_button2/dropdown_button2.dart';





class Research extends StatefulWidget {
  final stopInfo;
  final stopInfoString;
  Research({Key?key,this.stopInfo, this.stopInfoString}) : super(key: key);
  @override
  _ResearchState createState() =>  _ResearchState();
}



class _ResearchState extends State<Research> {

  String searchCity="no city chosen";
  String searchStop="no stop chosen";
  String research="";
  int flag=2;








  @override
  Widget build(BuildContext context) {


    List<DropdownMenuItem<String>> listCity=[];
    List<String> aux=[];



    listCity.add(const DropdownMenuItem(child: Text("no city chosen"),value: "no city chosen"));
    for (var item in widget.stopInfo){
      String check= item.getCity();
      if(!aux.contains(check)){
        listCity.add(DropdownMenuItem(child: Text(item.getCity()),value: item.getCity()));
        aux.add(check);
      }
    }



    void createDash()  {

        if(searchStop!="no stop chosen" && searchCity!="no city chosen"){
          for (var item in widget.stopInfo){
            String aux= item.getCity();
            if(aux.contains(searchCity) && item.getStop().contains(searchStop)){
              Navigator.of(context).push
                (MaterialPageRoute(builder: (context) =>
                  MainScreen(item: item,stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo,flag: flag,),));
            }
          }

        }
    }




    Stream< List<DropdownMenuItem<String>>> _clock(List<CityStopJson> stopInfo, String search) async* {
      List<DropdownMenuItem<String>> helperList=[];
      List<String> hList=[];

      helperList.add(const DropdownMenuItem(child: Text("no stop chosen"),value: "no stop chosen"));
      for (var item in stopInfo){
        String itemAux=item.toString().toLowerCase();
        String searchAux=search.toLowerCase();
        if(itemAux.contains(searchAux)&&!hList.contains(itemAux)){
          helperList.add( DropdownMenuItem(child: Text(item.getStop()),value: item.getStop()));
          hList.add(itemAux);
        }
      }
      yield helperList;
    }




    DataRow stopsDataRowDesign1() {

      return DataRow(
        cells: [
          DataCell( Expanded(
            child:
            DropdownButton2(
              items: listCity,
              value: searchCity,
              onChanged: (String? newValue) {
                setState(() {
                  searchStop="no stop chosen";
                  searchCity = newValue!;

                  if(searchCity!="no city chosen" || searchStop!="no stop chosen"){
                    research="Missing research details";
                  }else{
                    research="";
                  }
                });
              },
            ),),
          ),
          DataCell( Expanded(



            child: SizedBox(
              //width: double.infinity,
              // height: stopInfo.length.toDouble()*70,

              child:StreamBuilder(
                stream: _clock(widget.stopInfo, searchCity),
                builder: (context, AsyncSnapshot< List<DropdownMenuItem<String>>> snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CircularProgressIndicator();
                  }

                  return  DropdownButton(
                    items: snapshot.data,
                    value: searchStop,
                    onChanged: (String? newValue) {
                      setState(() {

                          searchStop = newValue!;

                          if(searchStop=="no stop chosen"){
                            research="Missing research details";
                          }
                          if(searchCity!="no city chosen" && searchStop!="no stop chosen"){
                            research=searchCity + "      "+ searchStop;
                          }

                      });
                    },
                  );
                },
              ),),





          ),

          ),
          DataCell(Center ( child :
          Ink(
            child: IconButton(
              padding: EdgeInsets.zero,
                icon: const Icon(FontAwesomeIcons.arrowRightLong, size: 40,),
                color: const Color(0xffFA8165),
                onPressed: () {
                  createDash();
                },

            ),
          ),),),
        ],

      );
    }




    DataRow stopsDR() {

      return DataRow(
        cells: [
          DataCell(
            DropdownButton(
              items: listCity,
              value: searchCity,
              onChanged: (String? newValue) {
                setState(() {
                  searchStop="no stop chosen";
                  searchCity = newValue!;

                  if(searchCity!="no city chosen" || searchStop!="no stop chosen"){
                    research="Missing research details";
                  }else{
                    research="";
                  }
                });
              },
          ),
          ),
          DataCell( SizedBox(
              //width: double.infinity,
              // height: stopInfo.length.toDouble()*70,

              child:StreamBuilder(
                stream: _clock(widget.stopInfo, searchCity),
                builder: (context, AsyncSnapshot< List<DropdownMenuItem<String>>> snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CircularProgressIndicator();
                  }

                  return  DropdownButton(
                    items: snapshot.data,
                    value: searchStop,
                    onChanged: (String? newValue) {
                      setState(() {

                        searchStop = newValue!;

                        if(searchStop=="no stop chosen"){
                          research="Missing research details";
                        }
                        if(searchCity!="no city chosen" && searchStop!="no stop chosen"){
                          research=searchCity + "      "+ searchStop;
                        }

                      });
                    },
                  );
                },
              ),),





          ),

          DataCell(Center ( child :
          Ink(
            child: IconButton(
              icon: const Icon(FontAwesomeIcons.arrowRightLong, size: 40,),
              color: const Color(0xffFA8165),
              onPressed: () {
                createDash();
              },

            ),
          ),),),
        ],

      );
    }



    DataCell esperimento1() {

      return
          DataCell(
            DropdownButton(
              items: listCity,
              value: searchCity,
              onChanged: (String? newValue) {
                setState(() {
                  searchStop="no stop chosen";
                  searchCity = newValue!;

                  if(searchCity!="no city chosen" || searchStop!="no stop chosen"){
                    research="Missing research details";
                  }else{
                    research="";
                  }
                });
              },
            ),
          );
    }



    DataCell esperimento2() {

      return
        DataCell( SizedBox(
          //width: double.infinity,
          // height: stopInfo.length.toDouble()*70,

          child:StreamBuilder(
            stream: _clock(widget.stopInfo, searchCity),
            builder: (context, AsyncSnapshot< List<DropdownMenuItem<String>>> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              }

              return  DropdownButton(
                items: snapshot.data,
                value: searchStop,
                onChanged: (String? newValue) {
                  setState(() {

                    searchStop = newValue!;

                    if(searchStop=="no stop chosen"){
                      research="Missing research details";
                    }
                    if(searchCity!="no city chosen" && searchStop!="no stop chosen"){
                      research=searchCity + "      "+ searchStop;
                    }

                  });
                },
              );
            },
          ),),





        );
    }




    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: const BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [

                if (!Responsive.isMobile(context))
                  Text( "Stops Fast Research", style: Theme.of(context).textTheme.headline6,),
                if (!Responsive.isMobile(context))
                  Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
              ],),

            const SizedBox(height: defaultPadding,),


            DataTable2(
              columnSpacing: defaultPadding,
              dataRowHeight: 80,
              minWidth: 600,
              showCheckboxColumn: false,
              showBottomBorder: false,
              bottomMargin: defaultPadding,
              horizontalMargin: defaultPadding,
              /*rows: List.generate(
                1,
                    (index) => stopsDR(),
              ),*/

              rows: [
                DataRow(cells: [esperimento1(), esperimento2(), DataCell(Center ( child :
                Ink(
                  child: IconButton(
                    icon: const Icon(FontAwesomeIcons.arrowRightLong, size: 40,),
                    color: const Color(0xffFA8165),
                    onPressed: () {
                      createDash();
                    },

                  ),
                ),),),]),
              ],

              columns:  [

                const DataColumn(
                  label: Text("City Selection", style: TextStyle(fontWeight: FontWeight.bold)),
                ),
                const DataColumn(
                  label: Text("Stop Selection", style: TextStyle(fontWeight: FontWeight.bold)),
                ),
                DataColumn(

                  label: Center(
                    widthFactor: 5.0, // You can set as per your requirement.
                    child: Text(research, textAlign: TextAlign.center, style: const TextStyle(fontWeight: FontWeight.bold, color: Color(0xffFA8165))),
                  ),),
              ],
            ),


            const SizedBox(height: defaultPadding,),




          ]),

    );

  }
}











