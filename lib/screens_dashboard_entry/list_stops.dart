import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:peoplecounter_dashoboard/screens_dashboard/dashboard_screen.dart';
import 'package:provider/provider.dart';
import '../jsonFilesHandling/cityStopJson.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import '../../../constants.dart';
import '../responsive.dart';
import '../screens_main/main_screen.dart';





  class ListStops extends StatefulWidget {
  final stopInfo;
  final stopInfoString;
  ListStops({Key?key,this.stopInfo, this.stopInfoString}) : super(key: key);
  @override
  _ListStopsState createState() =>  _ListStopsState();
  }



  class _ListStopsState extends State<ListStops> {

    String search="";
    int flag=1;


    Stream< List<CityStopJson>> _clock(List<CityStopJson> stopInfo, String search) async* {
      List<CityStopJson> helperList=[];
      for (var item in stopInfo){
        String itemAux=item.toString().toLowerCase();
        String searchAux=search.toLowerCase();
        if(!itemAux.contains(searchAux)){
          helperList.remove(item);
        }else{
          helperList.add(item);
        }
      }
      yield helperList;
    }


    @override
    Widget build(BuildContext context) {

      DataRow stopsDataRow(CityStopJson fileInfo, BuildContext context) {
        return DataRow(
          cells: [
            DataCell(Text(fileInfo.getCity(),), onTap: (){
              Navigator.of(context).push
              (MaterialPageRoute(builder: (context) =>
                  MainScreen(item: fileInfo,stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo,flag: flag,),));
          },),
            DataCell(Text(fileInfo.getStopNumber()), onTap: (){Navigator.of(context).push
              (MaterialPageRoute(builder: (context) =>
                MainScreen(item: fileInfo,stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo,flag: flag,),));
            },),
            DataCell(Text(fileInfo.getStopName()), onTap: (){Navigator.of(context).push
              (MaterialPageRoute(builder: (context) =>
                MainScreen(item: fileInfo,stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo, flag: flag,),));

            },),
          ],

        );
      }



      return Container(
        padding: const EdgeInsets.all(defaultPadding),
        decoration: const BoxDecoration(
          color: secondaryColor,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [

                  if (!Responsive.isMobile(context))
                    Text( "Stops List", style: Theme.of(context).textTheme.headline6,),
                  if (!Responsive.isMobile(context))
                    Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
                  Expanded(child: TextField(
                    keyboardType: TextInputType.text,
                    onChanged: (val) {
                      setState(() {
                        search = val;});
                      if (kDebugMode) {
                        print(search);
                      }
                    },
                    decoration: InputDecoration(
                      hintText: "Search",
                      fillColor: secondaryColor,
                      filled: true,
                      border: const OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      suffixIcon: Container(
                        padding: const EdgeInsets.all(defaultPadding * 0.75),
                        margin: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
                        decoration: const BoxDecoration(
                          color: Color(0xffFA8165),
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        child: const Icon(FontAwesomeIcons.magnifyingGlass, color: Colors.white70,),
                      ),
                    ),
                  ),),
                ],),

              const SizedBox(height: defaultPadding,),

              SizedBox(
                width: double.infinity,
                // height: stopInfo.length.toDouble()*70,

                child:StreamBuilder(
                  stream: _clock(widget.stopInfo, search),
                  builder: (context, AsyncSnapshot< List<CityStopJson>> snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    return  DataTable2(
                      columnSpacing: defaultPadding,
                      minWidth: 600,
                      rows: snapshot.data!.map((item) =>stopsDataRow(item, context)).toList(),
                      columns: const [
                        DataColumn(
                          label: Text("City", style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        DataColumn(
                          label: Text("Stop Number", style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        DataColumn(
                          label: Text("Stop Name", style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                      ],
                    );
                  },
                ),),
            ]),

      );

    }
  }

