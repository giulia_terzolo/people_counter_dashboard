import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
//import 'package:pie_chart/pie_chart.dart';
import '../../../constants.dart';
import 'package:fl_chart/fl_chart.dart';

import '../jsonFilesHandling/stopConstraints.dart';
import 'package:http/http.dart' as http;


class Chart extends StatefulWidget {
  final item;
  final stopInfo;
  final stopInfoString;

  Chart({Key?key,this.item, this.stopInfo, this.stopInfoString}) : super(key: key);
  @override
  _ChartState createState() =>  _ChartState();
}





class _ChartState extends State<Chart> {



  final gradientListGreen = Color.fromRGBO(129, 250, 112, 1);
  final gradientListYellow = Color.fromRGBO(250, 187, 58, 1.0);
  final gradientListRed = Color.fromRGBO(175, 63, 62, 1.0);



  @override
  void initState() {
    super.initState();
    setState(() {
      const oneSecond = const Duration(seconds: 25);
      new Timer.periodic(oneSecond, (Timer t) => setState((){}));
    });
  }

  @override
  Widget build(BuildContext context) {



    Future<StopInfoJson> fetchLimitCurrent(String city, String stopN) async{
      String urlLimits="https://f61c-93-39-144-110.eu.ngrok.io/stop_info?city="+city+"&stop_number="+stopN;
      String urlCurrent="https://f61c-93-39-144-110.eu.ngrok.io/n_people_now?city="+city+"&stop_number="+stopN;


      var responseLimits  = await http.get((Uri.parse(urlLimits)));
      var responseCurrent  = await http.get((Uri.parse(urlCurrent)));
      StopInfoJson sData = StopInfoJson(0.0, 0.0,0.0, 0.0);
      double limitYellow =0.0;
      double limitRed=0.0;
      double totalLimit=0.0;
      double current=0.0;

      if(responseLimits.statusCode==200){
        limitYellow = jsonDecode(responseLimits.body)["limitYellow"] as double;
        limitRed = jsonDecode(responseLimits.body)["limitRed"] as double;
        totalLimit = jsonDecode(responseLimits.body)["totalLimit"] as double;
      }
      if(responseCurrent.statusCode==200){
        current = jsonDecode(responseCurrent.body)["n_people"] as double;
      }

      sData = StopInfoJson(current, limitYellow,limitRed, totalLimit);

      return sData;
    }





    return FutureBuilder<StopInfoJson>(
      future: fetchLimitCurrent(widget.item.getCity(),widget.item.getStopNumber()),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Text("ERRORE");
        } else if (snapshot.hasData || snapshot.data != null) {
          Color gradientList;

          if(snapshot.data!.getCurrent()>snapshot.data!.getLimitRed()){
            gradientList=gradientListRed;
          }else if(snapshot.data!.getCurrent()> snapshot.data!.getLimitYellow()){
            gradientList=gradientListYellow;
          }else{
            gradientList=gradientListGreen;
          }

          double thrGr;
          double thrYe;
          double thrRe;

          if(snapshot.data!.getCurrent()<snapshot.data!.getLimitYellow()){
            thrGr=snapshot.data!.getLimitYellow()-snapshot.data!.getCurrent();
          }else{
            thrGr=0;
          }


          if(snapshot.data!.getCurrent()<snapshot.data!.getLimitRed()){
            thrYe=snapshot.data!.getLimitRed()-snapshot.data!.getCurrent()-thrGr;
          }else{
            thrYe=0;
          }


          if(snapshot.data!.getCurrent()<snapshot.data!.getTotalLimit()){
            thrRe=snapshot.data!.getTotalLimit()-snapshot.data!.getCurrent()-thrGr-thrYe;
          }else{
            thrRe=0;
          }


          if (kDebugMode) {
            print(thrGr);
            print(thrYe);
            print(thrRe);
            print(snapshot.data!.getCurrent());
            print(snapshot.data!.getTotalLimit());
          }

          return SizedBox(
            height: 200,
            child: Stack(
              alignment: Alignment.center,
              children: [
                PieChart(
                  PieChartData(
                    sectionsSpace: 0,
                    centerSpaceRadius: 70,
                    startDegreeOffset: -90,
                    sections:  [
                      PieChartSectionData(
                        color: gradientList,
                        value: snapshot.data!.getCurrent(),
                        showTitle: false,
                        radius: 35,
                      ),
                      PieChartSectionData(
                        color: Color.fromRGBO(129, 250, 112, 1).withOpacity(0.1),
                        value: thrGr,
                        showTitle: false,
                        radius: 30,
                      ),
                      PieChartSectionData(
                        color: Color.fromRGBO(250, 187, 58, 1.0).withOpacity(0.1),
                        value: thrYe,
                        showTitle: false,
                        radius: 30,
                      ),
                      PieChartSectionData(
                        color: Color.fromRGBO(175, 63, 62, 1.0).withOpacity(0.1),
                        value: thrRe,
                        showTitle: false,
                        radius: 30,
                      ),

                    ],
                  ),
                ),



                Positioned.fill(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: defaultPadding),
                      Text(
                        snapshot.data!.getCurrentInt().toString(),
                        style: Theme.of(context).textTheme.headline4!.copyWith(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          height: 0.5,
                        ),
                      ),
                      const Text("people"),
                    ],
                  ),
                ),
              ],
            ),
          );
        }

        return Container(
          height: 200,
          padding: EdgeInsets.all(defaultPadding),
          decoration: BoxDecoration(
            color: secondaryColor,
            borderRadius: const BorderRadius.all(Radius.circular(10)),
          ),
          child: const Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                    const Color(0xffFA8165),
                  ),
                ),
              ),


        );

      },
    );




      /*SizedBox(
      height: 200,
      child: Stack(
        alignment: Alignment.center,
        children: [
          PieChart(
            PieChartData(
              sectionsSpace: 0,
              centerSpaceRadius: 70,
              startDegreeOffset: -90,
              sections: paiChartSelectionDatas,
            ),
          ),

          /*PieChart(

            totalValue: widget.stopData.getTotalLimit(),
            dataMap: dataMap,
            animationDuration: Duration(milliseconds: 800),
            //chartLegendSpacing: 32,
            chartRadius: MediaQuery.of(context).size.width / 3.2,
            initialAngleInDegree: 90,
            chartType: ChartType.ring,
            ringStrokeWidth: 32,
            legendOptions: const LegendOptions(
              showLegendsInRow: true,
              legendPosition: LegendPosition.bottom,
              showLegends: false,
              legendTextStyle: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            chartValuesOptions: const ChartValuesOptions(
              showChartValueBackground: true,
              showChartValues: true,
              showChartValuesInPercentage: true,
              showChartValuesOutside: false,
              decimalPlaces: 1,
              chartValueStyle: TextStyle(
                fontWeight: FontWeight.bold, color: Color(0xFF2A2D3E)
              ),
              //chartValueBackgroundColor:Color(0xFF2A2D3E),
            ),
            baseChartColor: primaryColor.withOpacity(0.1),
            gradientList: gradientList,

            /*emptyColorGradient: [
              Colors.yellowAccent,
              Colors.blue,
            ],*/
          ),*/








          Positioned.fill(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: defaultPadding),
                Text(
                  widget.stopData.getCurrentInt().toString(),
                  style: Theme.of(context).textTheme.headline4!.copyWith(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    height: 0.5,
                  ),
                ),
                const Text("people"),
              ],
            ),
          ),
        ],
      ),
    );*/





  }
}
