import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import '../../../constants.dart';
import '../jsonFilesHandling/stopConstraints.dart';
import 'chart.dart';
import 'package:http/http.dart' as http;



class StarageDetails extends StatefulWidget {
  final item;
  final stopInfo;
  final stopInfoString;

  StarageDetails({Key?key,this.item, this.stopInfo, this.stopInfoString,
   }) : super(key: key);
  @override
  _StarageDetailsState createState() =>  _StarageDetailsState();
}



class _StarageDetailsState extends State<StarageDetails> {




  @override
  Widget build(BuildContext context) {


    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Current number & semaphore situation",
            style: Theme.of(context).textTheme.subtitle1,
          ),
          SizedBox(height: 25),
          Chart(item: widget.item,stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo,),
          SizedBox(height: 25),


        ],
      ),
    );




  }
}