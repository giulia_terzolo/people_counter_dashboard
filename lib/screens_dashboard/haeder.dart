import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../controllers/MenuController.dart';
import '../responsive.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../constants.dart';



class Header extends StatefulWidget {

  final item;
  final stopInfo;
  final stopInfoString;
  final flag;

  Header({Key?key,this.item, this.stopInfo, this.stopInfoString, this.flag}) : super(key: key);
  @override
  _HeaderState createState() =>  _HeaderState();
}



class _HeaderState extends State<Header> {


  @override
  Widget build(BuildContext context) {
    String title="Dashboard for " + widget.item.getTitle();
    return Row(
      children: [
        const SizedBox(width: defaultPadding,),
    Ink(
    decoration: ShapeDecoration(
    color: Color(0xFF212332),
    shape: CircleBorder(),
    ),
    child:
    IconButton(
      padding: EdgeInsets.zero,
      icon: const Icon(FontAwesomeIcons.arrowLeftLong, size: 40,),
      color: const Color(0xffFA8165),
      onPressed: () {
        //if(widget.flag==2){
          //Navigator.pop(context);}
        Navigator.pop(context);
        },
      ),),
        const SizedBox(width: defaultPadding,),
        if (!Responsive.isMobile(context))
          Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),

        if (!Responsive.isMobile(context))
          Text(
            title,
            style: Theme.of(context).textTheme.headline6,
          ),
        if (Responsive.isMobile(context))
          Text(
            "Dashboard",
            style: Theme.of(context).textTheme.headline6,
          ),



        if (!Responsive.isMobile(context))
          Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),



      ],
    );
  }
}

