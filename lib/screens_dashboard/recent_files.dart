import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import '../../../constants.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import '../jsonFilesHandling/chartDataJson.dart';


class RecentFiles extends StatefulWidget {
  final item;
  final stopInfo;
  final stopInfoString;


  RecentFiles({Key?key,this.item, this.stopInfo, this.stopInfoString}) : super(key: key);
  @override
  _RecentFilesState createState() =>  _RecentFilesState();
}



class _RecentFilesState extends State<RecentFiles> {


  late TooltipBehavior _tooltipBehavior;


  @override
  void initState() {
    _tooltipBehavior = TooltipBehavior(enable: true, header: "number of people");

    setState(() {
      const oneSecond = const Duration(seconds: 25);
      new Timer.periodic(oneSecond, (Timer t) => setState((){}));
    });

    super.initState();
  }



  Future<List<ChartData>> fetchChartData(String city, String stopN) async{
    String url="https://f61c-93-39-144-110.eu.ngrok.io/n_people?city="+city+"&stop_number="+stopN;
    var response = await http.get((Uri.parse(url)));
    List<ChartData> list= [];


    if(response.statusCode==200){
      var tagObjsJson = jsonDecode(response.body)['timePlotInfo'] as List;
      list = tagObjsJson.map((tagJson) => ChartData.fromJson(tagJson)).toList();
    }

    return list;
  }

  @override
  Widget build(BuildContext context) {



    final Color color1 = const Color(0xffFA696C);
    final Color color2 = const Color(0xffFA8165);
    final Color color3 = const Color(0xffFB8964);

    final List<Color> color = <Color>[];
    color.add(color3.withOpacity(0.6));
    color.add(color2.withOpacity(0.6));
    color.add(color1.withOpacity(0.6));





    final List<double> stops = <double>[];
    stops.add(0.0);
    stops.add(0.5);
    stops.add(1.0);

    final LinearGradient gradientColors =
    LinearGradient(colors: color, stops: stops);






    return  FutureBuilder<List<ChartData>>(
      future: fetchChartData(widget.item.getCity(),widget.item.getStopNumber()),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return  Text("${snapshot.error}");
        } else if (snapshot.hasData || snapshot.data != null) {
          return Container(
            width: double.infinity,
            padding: EdgeInsets.all(defaultPadding),
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Plot of the situation in the last 3 hour and current",
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                const SizedBox(height: defaultPadding,),
                SizedBox(
                  width: double.infinity,
                  child: SfCartesianChart(
                    // Enables the tooltip for all the series in chart
                      tooltipBehavior: _tooltipBehavior,
                      // Initialize category axis
                      primaryXAxis: CategoryAxis(
                        // Axis labels will be rotated to 90 degree
                          labelRotation: 90,
                      ),
                      series: <ChartSeries>[
                        // Initialize line series
                        SplineAreaSeries<ChartData, String>(
                          // Enables the tooltip for individual series
                          enableTooltip: true,
                          dataSource: snapshot.data!,
                          xValueMapper: (ChartData data, _) => data.timestamp,
                          yValueMapper: (ChartData data, _) => data.number,
                          //width: 0.1, // Width of the bars
                          //spacing: 0.1, // Spacing between the bars
                          //borderRadius: BorderRadius.all(Radius.circular(15)),
                          gradient: gradientColors,


                        )
                      ]
                  ),

                ),
              ],
            ),
          );
        }

        return Container(
          height: 305,
          padding: EdgeInsets.all(defaultPadding),
          decoration: BoxDecoration(
            color: secondaryColor,
            borderRadius: const BorderRadius.all(Radius.circular(10)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Plot of the situation in the last 3 hour and current",
                style: Theme.of(context).textTheme.subtitle1,
              ),
              SizedBox(height: 25),
              Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                    const Color(0xffFA8165),
                  ),
                ),
              ),
              //SizedBox(height: 25),


            ],
          ),
        );

      },
    );






      /*Container(
      width: double.infinity,
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Plot of the situation in the last 3 hour and current",
            style: Theme.of(context).textTheme.subtitle1,
          ),
          const SizedBox(height: defaultPadding,),
          SizedBox(
            width: double.infinity,
            child: SfCartesianChart(
              // Enables the tooltip for all the series in chart
                tooltipBehavior: _tooltipBehavior,
                // Initialize category axis
                primaryXAxis: CategoryAxis(
                  // Axis labels will be rotated to 90 degree
                    labelRotation: 90
                ),
                series: <ChartSeries>[
                  // Initialize line series
                  SplineAreaSeries<ChartData, String>(
                    // Enables the tooltip for individual series
                      enableTooltip: true,
                      dataSource: widget.tagObjs,
                      xValueMapper: (ChartData data, _) => data.timestamp,
                      yValueMapper: (ChartData data, _) => data.number,
                      //width: 0.1, // Width of the bars
                      //spacing: 0.1, // Spacing between the bars
                      //borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: gradientColors,


                  )
                ]
            ),

          ),
        ],
      ),
    );*/



  }
}
