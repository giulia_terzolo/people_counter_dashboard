import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:peoplecounter_dashoboard/jsonFilesHandling/chartDataJson.dart';

import '../jsonFilesHandling/stopConstraints.dart';
import '../responsive.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import 'googleMap.dart';
import 'haeder.dart';

import 'recent_files.dart';
import 'storage_details.dart';
import 'package:latlong2/latlong.dart' as latLng;
import 'package:http/http.dart' as http;


class DashboardScreen extends StatefulWidget {
  final stopInfo;
  final stopInfoString;
  final item;
  final flag;

  DashboardScreen({Key?key,this.stopInfo, this.stopInfoString, this.item, this.flag}) : super(key: key);
  @override
  _DashboardScreenState createState() =>  _DashboardScreenState();
}



class _DashboardScreenState extends State<DashboardScreen> {
  //latLng.LatLng position = latLng.LatLng(0.0, 0.0);
  //StopInfoJson stopData = StopInfoJson(0.0, 0.0, 0.0, 0.0);
  //List<ChartData> tagObjs = [];





  /*Future<StopInfoJson> fetchLimitCurrent(String city, String stopN) async{
    String urlLimits="https://f61c-93-39-144-110.eu.ngrok.io/stop_info?city="+city+"&stop_number="+stopN;
    String urlCurrent="https://f61c-93-39-144-110.eu.ngrok.io/n_people_now?city="+city+"&stop_number="+stopN;


    var responseLimits  = await http.get((Uri.parse(urlLimits)));
    var responseCurrent  = await http.get((Uri.parse(urlCurrent)));
    StopInfoJson sData = StopInfoJson(0.0, 0.0,0.0, 0.0);
    double limitYellow =0.0;
    double limitRed=0.0;
    double totalLimit=0.0;
    double current=0.0;

    if(responseLimits.statusCode==200){
      limitYellow = jsonDecode(responseLimits.body)["limitYellow"] as double;
      limitRed = jsonDecode(responseLimits.body)["limitRed"] as double;
      totalLimit = jsonDecode(responseLimits.body)["totalLimit"] as double;
    }
    if(responseCurrent.statusCode==200){
      current = jsonDecode(responseCurrent.body)["n_people"] as double;
    }
    sData = StopInfoJson(current, limitYellow,limitRed, totalLimit);

    return sData;
  }*/



  /*Future<List<ChartData>> fetchChartData(String city, String stopN) async{
    String url="https://f61c-93-39-144-110.eu.ngrok.io/n_people?city="+city+"&stop_number="+stopN;
    var response = await http.get((Uri.parse(url)));
    List<ChartData> list= [];


    if(response.statusCode==200){
      var tagObjsJson = jsonDecode(response.body)['timePlotInfo'] as List;
      list = tagObjsJson.map((tagJson) => ChartData.fromJson(tagJson)).toList();
    }

    return list;
  }*/

  /*latLng.LatLng position = latLng.LatLng(0.0, 0.0);

  Future<latLng.LatLng> fetchLatLng(String city, String stopN) async{
    String url="https://f61c-93-39-144-110.eu.ngrok.io/stop_info?city="+city+"&stop_number="+stopN;
    var response = await http.get((Uri.parse(url)));
    latLng.LatLng pos= latLng.LatLng(0.0, 0.0);

    if(response.statusCode==200){
      if (kDebugMode) {
        print(response.body);
      }
      double lat = jsonDecode(response.body)["lat"] as double;
      double lng = jsonDecode(response.body)["lng"] as double;
      pos = latLng.LatLng(lat, lng);
      if (kDebugMode) {
        print(pos);
      }
    }
    return pos;
  }*/



  /*fetchLatLng(widget.item.getCity(),widget.item.getStopNumber()).then((value){
      position.latitude=value.latitude;
      position.longitude=value.longitude;
      if (kDebugMode) {
        print(position);
      }
    });
    if (kDebugMode) {
      print(position);
    }*/

  @override
  Widget build(BuildContext context) {

    //String confFileCitiesStop='{"lat": 45.07071476812802, "lng": 7.656441450169924, "current": 20.0, "limitYellow": 5.0, "limitRed": 15.0, "totalLimit": 25.0}';
    /*double lat = jsonDecode(confFileCitiesStop)["lat"] as double;
    double lng = jsonDecode(confFileCitiesStop)["lng"] as double;
    latLng.LatLng position = latLng.LatLng(lat, lng);*/
    /*fetchLatLng(widget.item.getCity(),widget.item.getStopNumber()).then((value){
      position.latitude=value.latitude;
      position.longitude=value.longitude;
      if (kDebugMode) {
        print(position);
      }
    });*/




    /*double current = jsonDecode(confFileCitiesStop)["current"] as double;
    double limitYellow = jsonDecode(confFileCitiesStop)["limitYellow"] as double;
    double limitRed = jsonDecode(confFileCitiesStop)["limitRed"] as double;
    double totalLimit = jsonDecode(confFileCitiesStop)["totalLimit"] as double;
    StopInfoJson stopData = StopInfoJson(current, limitYellow,limitRed, totalLimit);*/

    /*fetchLimitCurrent(widget.item.getCity(),widget.item.getStopNumber()).then((value){
      stopData=value;
      if (kDebugMode) {
        print(position);
      }
    });*/
/*String arrayObjsText =
        '{"timePlotInfo": [{"timestamp": "12:30", "number": 3}, {"timestamp": "12:45", "number": 5}, {"timestamp": "13:00", "number": 5}, {"timestamp": "13:15", "number": 6}]}';
    var tagObjsJson = jsonDecode(arrayObjsText)['timePlotInfo'] as List;
    List<ChartData> tagObjs = tagObjsJson.map((tagJson) => ChartData.fromJson(tagJson)).toList();*/

    /*fetchChartData(widget.item.getCity(),widget.item.getStopNumber()).then((value){
      tagObjs.addAll(value);
      if (kDebugMode) {
        print(tagObjs.toString());
      }
    });*/




    return SafeArea(
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            Header(item: widget.item,stopInfo: widget.stopInfo,stopInfoString: widget.stopInfoString, flag: widget.flag,),
            SizedBox(height: defaultPadding),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Expanded(
                  flex: 5,
                  child: Column(
                    children: [
                      if (Responsive.isMobile(context)) StarageDetails(item: widget.item,stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo,),
                      if (Responsive.isMobile(context))
                        SizedBox(height: defaultPadding),
                      if (Responsive.isMobile(context)) RecentFiles(item: widget.item, stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo, ),
                      if (Responsive.isMobile(context))
                        SizedBox(height: defaultPadding),
                      GoogleMapScreen( item: widget.item,stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo,),
                      SizedBox(height: defaultPadding),



                    ],
                  ),
                ),





                if (!Responsive.isMobile(context))
                  SizedBox(width: defaultPadding),
                // On Mobile means if the screen is less than 850 we dont want to show it
                if (!Responsive.isMobile(context))
                  Expanded(
                    flex: 2,
                    child: StarageDetails(item: widget.item,stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo,),
                  ),
              ],
            ),
            if (!Responsive.isMobile(context))
              RecentFiles(item: widget.item, stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo, ),

          ],
        ),
      ),
    );
  }
}







