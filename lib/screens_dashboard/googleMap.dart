import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter/material.dart';
import '../../../constants.dart';
import 'package:latlong2/latlong.dart' as latLng;
import 'package:http/http.dart' as http;


class GoogleMapScreen extends StatefulWidget {
  final item;
  final stopInfo;
  final stopInfoString;

  GoogleMapScreen({Key?key,this.item, this.stopInfo, this.stopInfoString}) : super(key: key);
  @override
  _GoogleMapScreenState createState() =>  _GoogleMapScreenState();
}



class _GoogleMapScreenState extends State<GoogleMapScreen> {



  Future<latLng.LatLng> fetchLatLng(String city, String stopN) async{
    String url="https://f61c-93-39-144-110.eu.ngrok.io/stop_info?city="+city+"&stop_number="+stopN;
    var response = await http.get((Uri.parse(url)));
    latLng.LatLng pos= latLng.LatLng(0.0, 0.0);

    if(response.statusCode==200){
      if (kDebugMode) {
        print(response.body);
      }
      double lat = jsonDecode(response.body)["lat"] as double;
      double lng = jsonDecode(response.body)["lng"] as double;
      pos = latLng.LatLng(lat, lng);
      if (kDebugMode) {
        print(pos);
      }
    }
    return pos;
  }




  @override
  Widget build(BuildContext context) {



    return FutureBuilder<latLng.LatLng> (
      future: fetchLatLng(widget.item.getCity(),widget.item.getStopNumber()),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return const Text("ERRORE");
        } else if (snapshot.hasData || snapshot.data != null) {
          return Container(
            padding: const EdgeInsets.all(defaultPadding),
            decoration: const BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Map of the stop",
                  style: Theme.of(context).textTheme.subtitle1,
                ),





                SizedBox(
                  width: double.infinity,
                  height: 250,
                  child:

                  FlutterMap(
                    options: MapOptions(
                      minZoom: 10,
                      maxZoom: 18,
                      center: snapshot.data,
                      zoom: 18.0,
                    ),
                    layers: [
                      TileLayerOptions(
                        urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                        subdomains: ['a', 'b', 'c'],
                        attributionBuilder: (_) {
                          return Text("© OpenStreetMap contributors");
                        },
                      ),
                      MarkerLayerOptions(
                        markers: [
                          Marker(
                            width: 30.0,
                            height: 30.0,
                            point: snapshot.data!,
                            builder: (ctx) =>
                                Container(
                                  child: Icon(Icons.location_on,size: 30, color: Color(0xffFA696C),
                                  ),
                                ),
                          ),
                        ],
                      ),
                    ],
                  ),


                ),
              ],
            ),
          );
        }

        return Container(
          height: 305,
          padding: EdgeInsets.all(defaultPadding),
          decoration: BoxDecoration(
            color: secondaryColor,
            borderRadius: const BorderRadius.all(Radius.circular(10)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Map of the stop",
                style: Theme.of(context).textTheme.subtitle1,
              ),
              SizedBox(height: 25),
              Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                    const Color(0xffFA8165),
                  ),
                ),
              ),
              //SizedBox(height: 25),


            ],
          ),
        );

      },
    );





      /*Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: const BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Map of the stop",
            style: Theme.of(context).textTheme.subtitle1,
          ),





         SizedBox(
           width: double.infinity,
            height: 250,
            child:

            FlutterMap(
              options: MapOptions(
                minZoom: 10,
                maxZoom: 18,
                center: widget.position,
                zoom: 18.0,
              ),
              layers: [
                TileLayerOptions(
                  urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                  subdomains: ['a', 'b', 'c'],
                  attributionBuilder: (_) {
                    return Text("© OpenStreetMap contributors");
                  },
                ),
                MarkerLayerOptions(
                  markers: [
                    Marker(
                      width: 30.0,
                      height: 30.0,
                      point: widget.position,
                      builder: (ctx) =>
                          Container(
                            child: Icon(Icons.location_on,size: 30, color: Color(0xffFA696C),
                            ),
                          ),
                    ),
                  ],
                ),
              ],
            ),


         ),
        ],
      ),
    );*/



  }
}





















