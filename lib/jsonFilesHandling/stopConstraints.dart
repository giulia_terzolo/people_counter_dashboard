class StopInfoJson {
  double current;
  double limitYellow;
  double limitRed;
  double totalLimit;
  StopInfoJson(this.current, this.limitYellow, this.limitRed, this.totalLimit);


  double getCurrent() {
    return current;
  }


  int getCurrentInt() {
    return current.toInt();
  }
  double getLimitYellow() {
    return limitYellow;
  }

  double getLimitRed() {
    return limitRed;
  }

  double getTotalLimit() {
    return totalLimit;
  }


}
