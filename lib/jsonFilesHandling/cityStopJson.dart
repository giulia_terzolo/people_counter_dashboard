class CityStopJson {
  int stopNumber;
  String stopName;
  String cityName;
  CityStopJson(this.stopNumber, this.stopName, this.cityName);
  factory CityStopJson.fromJson(dynamic json) {
    return CityStopJson(json['stopNumber'] as int, json['stopName'] as String, json['cityName'] as String);
  }

  @override
  String toString() {
    return '{${cityName} - ${stopNumber} (${stopName}) }';
  }

  String getCity() {
    return '${cityName}';
  }

  String getStopNumber() {
    return '${stopNumber}';
  }

  String getStopName() {
    return '${stopName}';
  }


  String getStop() {
    return '${stopNumber}'+"   -    "+'${stopName}';
  }


  String getTitle() {
    return '${cityName}'+"   -    "+'${stopName}'+" ( "+'number ${stopNumber}'+" ) ";
  }

}
