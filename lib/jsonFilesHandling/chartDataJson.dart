class ChartData {
  String timestamp;
  int number;
  ChartData(this.timestamp, this.number);
  factory ChartData.fromJson(dynamic json) {
    return ChartData(json['timestamp'] as String, json['n_people'] as int);
  }

  @override
  String toString() {
    return '{ ${timestamp}, ${number} }';
  }

}
