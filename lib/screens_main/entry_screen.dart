import 'dart:convert';

import 'package:flutter/foundation.dart';

import '../controllers/MenuController.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../jsonFilesHandling/cityStopJson.dart';
import '../screens_dashboard_entry/dashboardentry_screen.dart';
import 'package:http/http.dart' as http;

class EntryScreen extends StatefulWidget {
  final stopInfo;
  final stopInfoString;

  EntryScreen({Key?key, this.stopInfo, this.stopInfoString}) : super(key: key);
  @override
  _EntryScreenState createState() =>  _EntryScreenState();
}



class _EntryScreenState extends State<EntryScreen> {

  /*late List<CityStopJson> stopInfo=[];
  late List<String> stopInfoString=[];



  Future<List<CityStopJson>> fetchCityStopJson() async{
    String url="https://f61c-93-39-144-110.eu.ngrok.io/all_stops";
    var response = await http.get((Uri.parse(url))).timeout(const Duration(seconds: 10));
    List<CityStopJson> stopI= [];
    if (kDebugMode) {
      print("dentro");
      print(response.body);
    }

    if(response.statusCode==200){
      var stopInfoJson = jsonDecode(response.body)["cityStop"] as List;
      stopI = stopInfoJson.map((elemJson) => CityStopJson.fromJson(elemJson)).toList();
    }

    return stopI;
  }*/






  @override
  Widget build(BuildContext context) {


   /* fetchCityStopJson().then((value){
      stopInfo.addAll(value);
    });

    for (final element in stopInfo) {
      stopInfoString.add(element.toString());
    }

    for (final element in stopInfoString) {
      if (kDebugMode) {
        print(element.toString());
      }
    }*/




    return Scaffold(
      //key: context.read<MenuController>().scaffoldKey,
      //drawer: SideMenu(),
      body: SafeArea(

        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            Expanded(
              // It takes 5/6 part of the screen
              flex: 5,
              child: DashEntryScreen(stopInfo: widget.stopInfo,stopInfoString: widget.stopInfoString,),
            ),
          ],
        ),
      ),
    );
  }
}













class EntryScreenHelper extends StatefulWidget {

  EntryScreenHelper({Key?key}) : super(key: key);
  @override
  _EntryScreenHelperState createState() =>  _EntryScreenHelperState();
}



class _EntryScreenHelperState extends State<EntryScreenHelper> {

  late List<CityStopJson> stopInfo=[];
  late List<String> stopInfoString=[];



  Future<List<CityStopJson>> fetchCityStopJson() async{
    String url="https://f61c-93-39-144-110.eu.ngrok.io/all_stops";
    var response = await http.get((Uri.parse(url))).timeout(const Duration(seconds: 10));
    List<CityStopJson> stopI= [];
    if (kDebugMode) {
      print("dentro");
      print(response.body);
    }

    if(response.statusCode==200){
      var stopInfoJson = jsonDecode(response.body)["cityStop"] as List;
      stopI = stopInfoJson.map((elemJson) => CityStopJson.fromJson(elemJson)).toList();
    }

    return stopI;
  }






  //Widget _body = CircularProgressIndicator();  // Default Body
  Widget _body = const Scaffold(body: Center(
  child: CircularProgressIndicator(
  valueColor: AlwaysStoppedAnimation<Color>(
    const Color(0xffFA8165),
  ),
  ),
  ),
  );

  @override
  void initState(){
    _gotoHomeScreen();
  }

  @override
  Widget build(BuildContext context){
    return _body;
  }

  Widget _gotoHomeScreen() {
    fetchCityStopJson().then((value){
      stopInfo.addAll(value);
      for (final element in stopInfo) {
        stopInfoString.add(element.toString());
      }

      for (final element in stopInfoString) {
        if (kDebugMode) {
          print(element.toString());
        }}
      setState(() => _body = EntryScreen(stopInfoString: stopInfoString, stopInfo: stopInfo,));
    });

    return _body;
  }


}

