import '../controllers/MenuController.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../screens_dashboard/dashboard_screen.dart';




class MainScreen extends StatefulWidget {
  final stopInfo;
  final stopInfoString;
  final item;
  final flag;

  MainScreen({Key?key, this.stopInfoString, this.stopInfo, this.item, this.flag}) : super(key: key);
  @override
  _MainScreenState createState() =>  _MainScreenState();
}



class _MainScreenState extends State<MainScreen> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      //key: context.read<MenuController1>().scaffoldKey,
      body: SafeArea(

        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [


            Expanded(
              // It takes 5/6 part of the screen
              flex: 5,
              child: DashboardScreen(item: widget.item, stopInfoString: widget.stopInfoString, stopInfo: widget.stopInfo,flag: widget.flag,),
            ),
          ],
        ),
      ),
    );
  }
}


