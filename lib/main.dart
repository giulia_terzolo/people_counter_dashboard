import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:chopper/chopper.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:peoplecounter_dashoboard/screens_main/entry_screen.dart';
import 'constants.dart';
import 'controllers/MenuController.dart';
import 'jsonFilesHandling/cityStopJson.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';




void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {



    return MaterialApp(

      debugShowCheckedModeBanner: false,
      title: 'People counter dashboard',
      theme: ThemeData.dark().copyWith(
        scaffoldBackgroundColor: bgColor,
        textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
            .apply(bodyColor: Colors.white),
        canvasColor: secondaryColor,
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => MenuController(),
          ),
          ChangeNotifierProvider(
            create: (context) => MenuController1(),
          ),
        ],

        child: EntryScreenHelper(),//MainScreen(),
      ),
    );
  }
}

